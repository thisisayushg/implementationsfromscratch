from dataset import train_loader, test_loader, BATCH_SIZE
from torch.optim.adam import Adam
from torch.optim.adadelta import Adadelta
from torch.optim.lr_scheduler import StepLR, ReduceLROnPlateau
from torch import nn, device, cuda, cat, uint8, randn, save, load
from torchvision.utils import make_grid
from kornia.color import lab_to_rgb
from torchsummary import summary
import torch
import sys
from model import ColorizationModel
from torchmetrics.image import PeakSignalNoiseRatio
from torch.utils.tensorboard import SummaryWriter
import os
from time import time

LEARNING_RATE = 0.01
EPOCHS = 11
ALPHA = 1/300

checkpoint = None
if os.path.exists('checkpoint/model.pt'):
    checkpoint = load('checkpoint/model.pt')

train_device = device("cuda" if cuda.is_available() else "cpu")
model = ColorizationModel()
model = model.to(train_device)
if checkpoint:
    model.load_state_dict(checkpoint['model'])


optimizer = Adadelta(params=model.parameters(), lr=LEARNING_RATE)
lr_scheduler = StepLR(optimizer, step_size=2, gamma=0.5)

if checkpoint:
    lr_scheduler.load_state_dict(checkpoint['optimizer'])

mse_criterion, crossentropy_criterion = nn.MSELoss(), nn.CrossEntropyLoss()

lr_scheduler = ReduceLROnPlateau(optimizer, factor=1/2, patience=3)
psnr = PeakSignalNoiseRatio(data_range=255)

writer = SummaryWriter(log_dir="rerun/colorization")
best_loss = float('inf')

resume_epoch = 0
if checkpoint:
    resume_epoch = checkpoint.get('epoch', 0)
    if resume_epoch != 0:
        resume_epoch += 1


for epoch in range(resume_epoch, EPOCHS):
    print(f"Resuming training from epoch {epoch} ")
    print(f"Optimizer learning rate {optimizer.param_groups[0]['lr']}")
    # print(f"Schduler props : Last epoch: {lr_scheduler.last_epoch}, learning rate: {lr_scheduler.get_last_lr()}")
    writer.add_scalar("Learning Rate", optimizer.param_groups[0]['lr'], global_step=epoch)
    model.train()
    model.to(train_device)
    batch_loss = 0
    processing_times=[]
    for idx, ((lightness_components, ab_components), target) in enumerate(train_loader):
        start_time = time()
        gray_img_batch = lightness_components.to(train_device)
        color_img_batch = ab_components.to(train_device)
        target = target.to(train_device)

        colorization_output, class_output = model(gray_img_batch)
        # print(f"output shape is {output.shape}")
        mse_loss = mse_criterion(colorization_output, color_img_batch)
        class_loss = crossentropy_criterion(class_output, target)
        total_loss = mse_loss + ALPHA * class_loss
        optimizer.zero_grad()
        total_loss.backward()
        optimizer.step()
        end_time = time()
        print(f"Loss for epoch {epoch} batch {idx+1} is {total_loss}, Time elapsed: {end_time - start_time}, LR: {optimizer.param_groups[0]['lr']}")
        batch_loss += total_loss.item()
        processing_times.append(end_time - start_time)
    
    batch_loss /= (idx+1)
    lr_scheduler.step()
    
    writer.add_scalar("Train Loss", batch_loss, global_step=epoch)
    for name, param in model.named_parameters():
        writer.add_histogram(name, param, global_step=epoch)

    writer.add_histogram("Batch training time", torch.tensor(processing_times), global_step=epoch)

    if batch_loss <= best_loss:
        best_loss = batch_loss
        save({
            'model': model.state_dict(),
            'scheduler': lr_scheduler.state_dict(),
            'epoch': epoch,
            'loss': batch_loss,
            'optimizer': optimizer.state_dict()
        }, 'checkpoint/model.pt')

    # checkpoint = load('checkpoint/model.pt')
    # loss1 = checkpoint['loss']
    # print(f"Loading model with best loss {loss1:.3f} saved in epoch {checkpoint['epoch']}")
    # model.load_state_dict(checkpoint['model'])

    model.eval()

    (lightness_components, ab_components), target = next(iter(test_loader))
    lightness_components = lightness_components.to(train_device)
    gray_samples = lightness_components*255/100
    predicted_ab_comps, _ = model(lightness_components)
    gray_samples = gray_samples.to(device=device('cpu'), dtype=uint8)
    gray_plot = make_grid(gray_samples)
    writer.add_image("gray image", img_tensor=gray_plot, global_step=epoch)

    predicted_samples = cat([lightness_components, predicted_ab_comps], dim=1)
    color_samples = cat([lightness_components, ab_components.to(train_device)], dim=1)
    # predicted_rgb_samples = lab_to_rgb(predicted_samples.to(device('cpu'))).to(dtype=uint8)
    color_rgb_samples = lab_to_rgb(color_samples.to(device('cpu')))
    color_rgb_samples = color_rgb_samples*255
    color_rgb_samples = color_rgb_samples.to(device=device('cpu'), dtype=uint8)

    predicted_rgb_samples = lab_to_rgb(predicted_samples.to(device('cpu')))
    predicted_rgb_samples = predicted_rgb_samples*255
    predicted_rgb_samples = predicted_rgb_samples.to(device=device('cpu'), dtype=uint8)
    # plt.imshow(gray_plot.permute(1, 2, 0))
    produced_color_plot = make_grid(predicted_rgb_samples)
    real_color_plot = make_grid(color_rgb_samples)

    # plt.imshow(color_plot.permute(1, 2, 0))
    writer.add_image("real color image", img_tensor=real_color_plot, global_step=epoch)
    writer.add_image("produced color image", img_tensor=produced_color_plot, global_step=epoch)


    test_loss = mse_criterion(predicted_rgb_samples.to(torch.float), color_rgb_samples.to(torch.float))
    psnr.update(predicted_rgb_samples, color_rgb_samples)
    psnr_value = psnr.compute()
    writer.add_scalar("PSNR", psnr_value, global_step=epoch)
    writer.add_scalar("Test Loss", test_loss, global_step=epoch)

writer.close()
import torch
from torch import nn
from dataset import train_data

# print(f'targets are {train_loader.dataset.targets}')
NUM_CLASSES = len(train_data.class_to_idx)
print(f'Found {NUM_CLASSES} classes in training dataset')
class ConvBlock(nn.Module):
    def __init__(
        self, in_channels, out_channels, dilation=1, padding=1, stride=2
    ) -> None:
        super().__init__()

        self.model = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=3,
                stride=stride,
                padding=padding,
                dilation=dilation,
            ),
            nn.ReLU(),
            nn.BatchNorm2d(out_channels),
        )

    def forward(self, x):
        return self.model(x)


class UpsampleBlock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.model = nn.Sequential(
            nn.ConvTranspose2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=4,
                stride=2,
                padding=1,
            ),
            nn.ReLU(),
            nn.BatchNorm2d(out_channels),
        )

    def forward(self, x):
        return self.model(x)


class LLNetwork(nn.Module):
    def __init__(
        self,
    ) -> None:
        super().__init__()
        first_out = 64
        self.model = nn.Sequential(
            ConvBlock(in_channels=1, out_channels=first_out),
            ConvBlock(
                in_channels=first_out, out_channels=2 * first_out, stride=1
            ),
            ConvBlock(in_channels=2 * first_out, out_channels=2 * first_out),
            ConvBlock(
                in_channels=2 * first_out, out_channels=4 * first_out, stride=1
            ),
            ConvBlock(in_channels=4 * first_out, out_channels=4 * first_out),
            ConvBlock(
                in_channels=4 * first_out, out_channels=8 * first_out, stride=1
            ),
        )

    def forward(self, x):
        return self.model(x)


class MLNetwork(nn.Module):
    def __init__(self):
        super().__init__()
        first_out = 64

        self.model = nn.Sequential(
            ConvBlock(
                in_channels=8 * first_out, out_channels=8 * first_out, stride=1
            ),
            ConvBlock(
                in_channels=8 * first_out, out_channels=4 * first_out, stride=1
            ),
        )

    def forward(self, x):
        return self.model(x)


class GlobalFeaturesNetwork(nn.Module):
    def __init__(self):
        super().__init__()
        first_out = 64

        self.classification_out = nn.Sequential(
            ConvBlock(in_channels=8 * first_out, out_channels=8 * first_out),
            ConvBlock(
                in_channels=8 * first_out, out_channels=8 * first_out, stride=1
            ),
            ConvBlock(in_channels=8 * first_out, out_channels=8 * first_out),
            ConvBlock(
                in_channels=8 * first_out, out_channels=8 * first_out, stride=1
            ),
            nn.Flatten(),
            nn.Linear(7 * 7 * 8 * first_out, 2 * 8 * first_out),
            nn.Linear(2 * 8 * first_out, 8 * first_out),
        )
        self.full_model = nn.Sequential(
            self.classification_out, nn.Linear(8 * first_out, 4 * first_out)
        )

    def forward(self, x):
        return self.classification_out(x), self.full_model(x)


class ColorizationNet(nn.Module):
    def __init__(self, out_channels):
        super().__init__()
        first_out = 64
        self.model = nn.Sequential(
            ConvBlock(
                in_channels=8 * first_out, out_channels=2 * first_out, stride=1
            ),
            UpsampleBlock(
                in_channels=2 * first_out, out_channels=2 * first_out
            ),
            ConvBlock(
                in_channels=2 * first_out, out_channels=first_out, stride=1
            ),
            ConvBlock(
                in_channels=first_out, out_channels=first_out, stride=1
            ),
            UpsampleBlock(in_channels=first_out, out_channels=first_out),
            ConvBlock(
                in_channels=first_out, out_channels=int(first_out / 2), stride=1
            ),
            ConvBlock(
                in_channels=int(first_out / 2), out_channels=out_channels, stride=1
            ),
            UpsampleBlock(in_channels=out_channels, out_channels=out_channels),
        )

    def forward(self, x):
        return self.model(x)


class ColorizationModel(nn.Module):
    def __init__(self):
        super().__init__()
        first_out = 64
        self.mlnet = MLNetwork()
        self.llnet = LLNetwork()
        self.gfnet = GlobalFeaturesNetwork()
        self.colornet = ColorizationNet(2)
        self.classnet = nn.Sequential(
            nn.Linear(8*first_out, 4*first_out),
            nn.Linear(4*first_out, NUM_CLASSES),
            nn.Softmax()
        )

    def forward(self, x):
        # output = self.classnet(x)
        ll_features = self.llnet(x)
        ml_features = self.mlnet(ll_features)
        g_features = self.gfnet(ll_features)
        a = g_features[1].unsqueeze(-1).repeat(1, 1, 28).unsqueeze(-1).repeat(1, 1, 1, 28)
        fusion = torch.cat([ml_features, a], dim=1)
        
        return self.colornet(fusion), self.classnet(g_features[0])

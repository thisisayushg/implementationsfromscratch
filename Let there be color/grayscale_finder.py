from functools import partial
from torchvision.utils import make_grid
import torchvision.transforms.functional as F
import matplotlib.pyplot as plt
import numpy as np
from torchvision.datasets import ImageFolder
from torchvision.transforms import Compose, RandomCrop, ToTensor
from torch.utils.data import DataLoader, Subset
from kornia.color import rgb_to_lab
from torch import log10, floor, abs
import torch
from torchvision.io import read_image


def split_to_lab(img):
    image = rgb_to_lab(img)
    # The color image consists of the 'a' and 'b' parts of the LAB format.
    A = image[1, :, :].unsqueeze(0)
    B = image[2, :, :].unsqueeze(0)
    # The gray image consists of the `L` part of the LAB format.
    gray_image = image[0, :, :].unsqueeze(0)

    return gray_image, A, B


class SplitLABComponents(object):
    def __call__(self, img):
        image = rgb_to_lab(img)
        # The color image consists of the 'a' and 'b' parts of the LAB format.
        # The gray image consists of the `L` part of the LAB format.
        # gray_image = image[0, :, :].unsqueeze(0)
        C = image[1:, :, :]
        return img, torch.var(C, dim=1).mean()


def find_exp(tensor) -> int:
    base10 = log10(abs(tensor))
    return floor(base10)


def validate_variance(file_ptr, path):
    image = read_image(path)
    image = rgb_to_lab(image)
    # The color image consists of the 'a' and 'b' parts of the LAB format.
    A = image[1, :, :].unsqueeze(0)
    B = image[2, :, :].unsqueeze(0)
    # The gray image consists of the `L` part of the LAB format.
    gray_image = image[0, :, :].unsqueeze(0)
    C = image[1:, :, :]
    variance = torch.var(C, dim=1).mean()
    min_indices = torch.argmin(variance)
    magnitude = find_exp(variance.min())
    if magnitude in [-4, -5, -6, -7]:
        file_ptr.write(f"{path}\n")
        print(path)


def show(imgs):
    if not isinstance(imgs, list):
        imgs = [imgs]

    fig, axs = plt.subplots(ncols=len(imgs), squeeze=False)

    for i, img in enumerate(imgs):
        img = img.detach()
        img = F.to_pil_image(img)
        axs[0, i].imshow(np.asarray(img))
        axs[0, i].set(xticklabels=[], yticklabels=[], xticks=[], yticks=[])
    return


BATCH_SIZE = 128

with open("grayscale_imgs.txt", "w") as file:
    filter_grayscale = partial(validate_variance, file)

    train_data = ImageFolder(
        root="Places365/train/", is_valid_file=filter_grayscale
    )
    train_loader = DataLoader(dataset=train_data, shuffle=True, batch_size=BATCH_SIZE)

    for (imgs, variance), _ in train_loader:
        ...

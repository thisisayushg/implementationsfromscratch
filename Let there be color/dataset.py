from torchvision.datasets import ImageFolder
from torchvision.transforms import Compose, RandomCrop, ToTensor
from torch.utils.data import DataLoader, Subset
from kornia.color import rgb_to_lab
from numpy import arange
from sklearn.model_selection import train_test_split
from torch import log10, floor, abs
from torchvision.io import read_image
import torch
from functools import partial
import os


BATCH_SIZE = 192

class SplitLABComponents(object):
    def __call__(self, img):
        image = rgb_to_lab(img)

        # The color image consists of the 'a' and 'b' parts of the LAB format.
        color_image = image[1:, :, :]
        # The gray image consists of the `L` part of the LAB format.
        gray_image = image[0, :, :].unsqueeze(0)

        return gray_image, color_image


with open("grayscale_imgs.txt", "r") as file:
    _ = file.readlines()
    content = [line.strip('\n').strip() for line in _]

def file_not_in_grayscale_imgs(content, path):
    if os.path.abspath(path) in content:
        print(f'File {path} not a valid image')
        return False
    return True

is_colorful = partial(file_not_in_grayscale_imgs, content)


train_dataset = ImageFolder(
    root="Places365/train/",
    transform=Compose([ToTensor(), RandomCrop((224, 224)), SplitLABComponents()]),
    is_valid_file=is_colorful,
)

test_dataset = ImageFolder(
    root="Places365/val/",
    transform=Compose([ToTensor(), RandomCrop((224, 224)), SplitLABComponents()]),
)


# train_idx, _ = train_test_split(arange(len(train_dataset)), train_size=0.001, random_state=100, shuffle=True, stratify=train_dataset.targets)
# _, test_idx = train_test_split(arange(len(test_dataset)), train_size=0.99, random_state=100, shuffle=True, stratify=test_dataset.targets)
# train_data = Subset(train_dataset, train_idx)
# test_data = Subset(test_dataset, test_idx)

train_data = train_dataset
test_data = test_dataset

print(f"Length of train dataset is {len(train_data)}")
print(f"Length of test dataset is {len(test_data)}")
print(f"Total batches to process {len(train_data)//BATCH_SIZE}")

train_loader = DataLoader(dataset=train_data, shuffle=True, batch_size=BATCH_SIZE)
test_loader = DataLoader(dataset=test_data, shuffle=True, batch_size=32)

from __future__ import annotations
import skimage
from skimage.segmentation import felzenszwalb
import cv2
import numpy
from typing import List, Tuple, Union, Optional
import random


def get_texture(img: numpy.ndarray):
    """
    Compute texture of the image based using the Local Binary Pattern algorithm

    Parameters
    ----------
    img: numpy.ndarray
        Image to compute the texture

    Returns
    -------
    numpy.ndarray
        Numpy ndarray with channel-specific textures
    """
    empty_tensor = numpy.zeros_like(img)

    for channel in range(3):
        empty_tensor[:, :, channel] = skimage.feature.local_binary_pattern(
            img[:, :, channel], 8, 1
        )

    return empty_tensor


class Region:
    """
    A class to represent an image region

    Attributes
    ----------
    label: str
        A unique identifier for the region
    region_mat: numpy.ndarray
        Multichannel array representation of the region extracted from the given image
    color_descriptor: numpy.ndarray
        1D array of representation of an image color features
    texture_descriptor: numpy.ndarray
        1D array of representation of an image texture features
    image_size: float
        area of the region
    bounds: dict
        Coordinates of the bounding box of the region. Only diagonal coordinates are needed: (min_x, min_y) and (max_x, max_y)

    Methods
    -------
    color_sim(region: Region)
        Compute the similarity of self with the given region parameter based on color descriptor
    texture_sim(region: Region)
        Compute the similarity of self with the given region parameter based on texture descriptor
    size_sim(region: Region)
        Compute the similarity of self with the given region parameter based on region sizes
    fill_sim(region: Region)
        Compute the similarity of self with the given region parameter based on fill descriptor
    similarity(region: Region)
        Compue the similarity of self with the given region based on fill similarity, color similarity,
        texture similarity and size similarity
    """

    def __init__(
        self,
        region_area: numpy.ndarray,
        label: str,
        image_texture: Optional[numpy.ndarray] = None,
        image: Optional[numpy.ndarray] = None,
        image_size: Optional[float] = None,
        color_descriptor: Optional[numpy.ndarray] = None,
        texture_descriptor: Optional[numpy.ndarray] = None,
        compute_descriptors: Optional[bool] = True,
    ):
        """
        Parameters
        ----------
        region_area: numpy.ndarray
            multichannel array representation of the image region
        image: numpy.ndarray
            Multichannel array representation of the whole image
        label: str
            A unique identifier for the region
        image_texture: numpy.ndarray
            Multichannel texture image, with texture computed for each channel
        color_descriptor: numpy.ndarray
            1D array of representation of an image color features
        texture_descriptor: numpy.ndarray
            1D array of representation of an image texture features
        """
        self.label = str(label)
        self.region_mat = region_area
        if color_descriptor is not None:
            self.color_descriptor = color_descriptor
        else:
            if compute_descriptors:
                self.color_descriptor = self._descriptor(self.region_mat, 25)

        self._texture = image_texture

        if texture_descriptor is not None:
            self.texture_descriptor = texture_descriptor
        else:
            if compute_descriptors:
                self.texture_descriptor = self._descriptor(self._texture, 10)

        if image_size:
            self.image_size = image_size
        elif image is not None:
            self.image_size = image.shape[0] * image.shape[1]

        self._bounds = {}
        self.neighbours = None
        self.conf_score = 0
        self.level = 1
        self.category: Union[None, str] = None

    def __str__(self):
        return f"Region {self.label}"

    @property
    def bounds(self):
        return self._bounds

    @bounds.setter
    def bounds(self, bounds: dict):
        self._bounds = bounds
        self.size = self._compute_region_size()
        self.width = self._bounds["max_x"] - self._bounds["min_x"]
        self.height = self._bounds["max_y"] - self._bounds["min_y"]

    def color_sim(self, region: Region) -> float:
        """
        Compute similarity based on color descriptor with the given region

        Parameters
        ----------
        region: Region
            Region to compute the color similarity with

        Returns
        -------
        float
            color similarity score
        """
        return numpy.sum(
            numpy.min(
                numpy.vstack((self.color_descriptor, region.color_descriptor)), axis=0
            )
        )

    def texture_sim(self, region: Region) -> float:
        """
        Compute similarity based on texture descriptor with the given region

        Parameters
        ----------
        region: Region
            Region to compute the texture similarity with

        Returns
        -------
        float
            texture similarity score
        """
        return numpy.sum(
            numpy.min(
                numpy.vstack((self.texture_descriptor, region.texture_descriptor)),
                axis=0,
            )
        )

    def size_sim(self, region: Region) -> float:
        """
        Compute similarity with the given region based on size of the regions

        Parameters
        ----------
        region: Region
            Region to compute the size similarity with

        Returns
        -------
        float
            size similarity score
        """
        return 1 - (self.size + region.size) / self.image_size

    def fill_sim(self, region: Region) -> float:
        """
        Compute similarity with the given region based on fill of the regions

        Parameters
        ----------
        region: Region
            Region to compute the fill similarity with

        Returns
        -------
        float
            fill similarity score
        """
        boxsize = (
            max(self.bounds["max_x"], region.bounds["max_x"])
            - min(self.bounds["min_x"], region.bounds["min_x"])
        ) * (
            max(self.bounds["max_y"], region.bounds["max_y"])
            - min(self.bounds["min_y"], region.bounds["min_y"])
        )
        return 1 - (boxsize - self.size - region.size) / self.image_size

    def similarity(self, region: Region) -> float:
        """
        Compute similarity with the given region

        Parameters
        ----------
        region: Region
            Region to compute the similarity with

        Returns
        -------
        float
            similarity score
        """
        return (
            self.color_sim(region)
            + self.texture_sim(region)
            + self.size_sim(region)
            + self.fill_sim(region)
        )

    def _descriptor(self, tensor: numpy.ndarray, bins: int) -> numpy.ndarray:
        descriptor = numpy.array([])
        channel = 0
        for channel in range(3):
            histogram, bin_boundaries = numpy.histogram(tensor[:, :, channel], bins)
            descriptor = numpy.append(descriptor, histogram)
        return descriptor

    def _compute_region_size(self) -> float:
        return (self._bounds["max_x"] - self._bounds["min_x"]) * (
            self._bounds["max_y"] - self._bounds["min_y"]
        )

    def is_neighbor_to(self, region: Region):
        b1 = self.bounds
        b2 = region.bounds

        if (
            (
                b1["min_x"] < b2["min_x"] < b1["max_x"]
                and b1["min_y"] < b2["min_y"] < b1["max_y"]
            )
            or (  # means r2 left top corner lies within the r1's horizontal limits
                b1["min_x"] < b2["max_x"] < b1["max_x"]
                and b1["min_y"] < b2["min_y"] < b1["max_y"]
            )
            or (  # means r2 right top corner lies within the r1's horizontal limits
                b1["min_x"] < b2["min_x"] < b1["max_x"]
                and b1["min_y"] < b2["max_y"] < b1["max_y"]
            )
            or (  # means r2 region's bottom lies within the r1's vertical limits
                b1["min_x"] < b2["max_x"] < b1["max_x"]
                and b1["min_y"] < b2["max_y"] < b1["max_y"]
            )  # means r2 region's top lies within r1's vertical limits
        ):
            return True
        return False


def generate_regions(image: numpy.ndarray) -> List[Region]:
    """
    Generate initial region segments using the `felzenszwalb` algorithm given an input image

    Parameters
    ----------
    image: numpy.ndarray
        Image to generate initial region segments onto

    Returns
    -------
    List[Region]
        A list of initial region segments
    """
    # seed random regions
    mask = felzenszwalb(image, min_size=50)
    image_texture = get_texture(image)
    R = {}

    for y, h_section in enumerate(image):
        for x, v_section in enumerate(h_section):
            l = mask[y, x]
            if R.get(l, 0) == 0:
                R[l] = {
                    "min_x": float("inf"),
                    "min_y": float("inf"),
                    "max_x": 0,
                    "max_y": 0,
                }

            if x < R[l]["min_x"]:
                R[l]["min_x"] = x
            if x > R[l]["max_x"]:
                R[l]["max_x"] = x

            if y < R[l]["min_y"]:
                R[l]["min_y"] = y
            if y > R[l]["max_y"]:
                R[l]["max_y"] = y

    all_regions = []
    for key, value in R.items():
        if value["min_x"] == value["max_x"] or value["min_y"] == value["max_y"]:
            continue
        region_area = image[
            value["min_y"] : value["max_y"], value["min_x"] : value["max_x"]
        ]
        texture_area = image_texture[
            value["min_y"] : value["max_y"], value["min_x"] : value["max_x"]
        ]

        reg = Region(
            region_area=region_area, image=image, label=key, image_texture=texture_area
        )
        reg.bounds = value

        all_regions.append(reg)

    return all_regions


def compute_region_neighbours(regions):
    """
    Find neighbours of each region

    Parameters
    ----------
    regions: List[Region]
        A list of regions to find the neighbour of each region

    Returns
    -------
    Tuple[List[Tuple[Region, Region]], List[float]]
        (List of Region pairs, each pair denoting two neighbouring regions, List of similarities corresponding to each region)
    """
    neighbours, similarities = [], []

    for i, r1 in enumerate(regions):
        b1 = r1.bounds
        for r2 in regions[i + 1 :]:
            b2 = r2.bounds

            if (
                (
                    b1["min_x"] < b2["min_x"] < b1["max_x"]
                    and b1["min_y"] < b2["min_y"] < b1["max_y"]
                )
                or (  # means r2 left top corner lies within the r1's horizontal limits
                    b1["min_x"] < b2["max_x"] < b1["max_x"]
                    and b1["min_y"] < b2["min_y"] < b1["max_y"]
                )
                or (  # means r2 right top corner lies within the r1's horizontal limits
                    b1["min_x"] < b2["min_x"] < b1["max_x"]
                    and b1["min_y"] < b2["max_y"] < b1["max_y"]
                )
                or (  # means r2 region's bottom lies within the r1's vertical limits
                    b1["min_x"] < b2["max_x"] < b1["max_x"]
                    and b1["min_y"] < b2["max_y"] < b1["max_y"]
                )
            ):  # means r2 region's top lies within r1's vertical limits
                similarities.append(r1.similarity(r2))
                neighbours.append((r1, r2))
    return neighbours, similarities


def merge_regions(image, r1, r2, image_texture):
    """
    Merge two regions and create a new region

    Parameters
    ----------
    image: numpy.ndarray
        Image on which r1 and r2 are located
    r1, r2: Region
        Regions on the given image to merge
    image_texture: numpy.ndarray
        Image converted as texture map

    Returns
    -------
    Region
        A region created by merging the two given regions
    """
    min_x = min((r1.bounds["min_x"], r2.bounds["min_x"]))
    min_y = min((r1.bounds["min_y"], r2.bounds["min_y"]))

    max_x = max((r1.bounds["max_x"], r2.bounds["max_x"]))
    max_y = max((r1.bounds["max_y"], r2.bounds["max_y"]))

    region_area = image[min_y:max_y, min_x:max_x]
    color_descriptor = (
        r1.size * r1.color_descriptor + r2.size * r2.color_descriptor
    ) / (r1.size + r2.size)

    texture_descriptor = (
        r1.size * r1.texture_descriptor + r2.size * r2.texture_descriptor
    ) / (r1.size + r2.size)

    reg = Region(
        region_area=region_area,
        image=image,
        label=r1.label + "_" + r2.label,
        image_texture=image_texture,
        color_descriptor=color_descriptor,
        texture_descriptor=texture_descriptor,
    )
    reg.level = max(r1.level, r2.level) + 1
    reg.bounds = {"min_x": min_x, "max_x": max_x, "min_y": min_y, "max_y": max_y}

    return reg


def region_neighbours(region, regions):
    """
    Find neighbours of a given region

    Parameters
    ----------
    region: Region
        A region whose neighbours are to be identified

    regions: Regions[]
        An array of regions to filter the regions neighbours to (overlapping with) the given region

    Returns
    -------
    Tuple[List[Tuple[Region, Region]], List[float]]
        (List of Region pairs, each pair denoting two neighbouring regions, List of similarities corresponding to each region)
    """
    neighbours, similarities = [], []

    for r in regions:
        if region.is_neighbor_to(r):
            similarities.append(region.similarity(r))
            neighbours.append((region, r))

    return neighbours, similarities


def coalesce(
    image: numpy.ndarray,
    regions,
    neighbours: List[Tuple[Region, Region]],
    similarities: List[float],
):
    """
    Merge the regions to create larger regions based on the similarities

    Parameters
    ----------
    regions: List[Region]
        List of regions to merge
    neighbours: List[Tuple[Region, Region]]
        list of Region pairs which are neighbours to each other
    similarities: List[int]
        list of similarity scores corresponding to each region pair in the `neighbours` parameter

    Returns
    -------
    list of Region
        list of non-neighbouring regions after merge
    """
    # get per-channel texture of the image
    image_texture = get_texture(image)

    # till there is any region pair with similarity
    while len(similarities) > 0:
        # find the index of the pair with max similarity
        max_item_idx = numpy.argmax(similarities)
        # get the region pair with max similarity
        r1, r2 = neighbours[max_item_idx]

        # remove this region pair and similarity
        neighbours.pop(max_item_idx)
        similarities.pop(max_item_idx)

        # merge the above region pairs to form a new region
        new_region = merge_regions(image, r1, r2, image_texture)

        old_region_neighbours = []
        # for each region pair which are neighbours of each other
        for idx, (a, b) in enumerate(neighbours):
            # if the first region of the pair matches either of the max similarity regions
            if a.label == r1.label or a.label == r2.label:
                # save first region for later
                old_region_neighbours.append(a)
                # remove them from the neighbours and similarities array
                neighbours.pop(idx)
                similarities.pop(idx)
            # if the second region of the pair matches either of the max similarity regions
            elif b.label == r2.label or b.label == r1.label:
                # save second region for later
                old_region_neighbours.append(b)
                # remove them from the neighbours and similarities array
                neighbours.pop(idx)
                similarities.pop(idx)

        # update all-regions list to remove the regions with the max similarity
        regions = [r for r in regions if r.label != r1.label and r.label != r2.label]

        # find neighbours of the new region
        n, s = region_neighbours(new_region, old_region_neighbours)
        # add new region to the all-regions list
        regions.append(new_region)

        # update neighbours and similarities list to include the new found
        # neighbours and similarities of the new merged region
        neighbours = neighbours + n
        similarities = similarities + s

    return regions


def selective_search(image: numpy.ndarray):
    """
    Given an image, perform selective search on it to return the filtered list of regions

    Parameters
    ----------
    image: numpy.ndarray
        Image to run selective search on

    Returns
    -------
    List[Region]
        a list of regions
    """
    R = generate_regions(image)
    neighbours, similarities = compute_region_neighbours(R)
    final_regions = coalesce(image, R, neighbours, similarities)
    # compute confidence score for each region. This score tells the confidence of having an object within that region
    for r in final_regions:
        r.conf_score = random.random() * r.level

    return final_regions

from selective_search import Region
from typing import List


def IOU(a: Region, b: Region):
    """
    Computes the Intersection-Over-Union between two regions

    Parameters
    ----------
    a, b: Region
        Two regions to compute IoU score between

    Returns
    -------
        IoU score of the two regions
    """
    r1 = a.bounds
    r2 = b.bounds
    height = r1["max_y"] - r2["min_y"]
    width = 0

    if r2["max_y"] > r1["min_y"]:
        height = r2["max_y"] - r1["min_y"]

    if r1["max_x"] > r2["min_x"]:
        # means the r2 is to the right of r1
        width = r1["max_x"] - r2["min_x"]

    if r2["max_x"] > r1["min_x"]:
        # means the r2 is to the left of r1
        width = r2["max_x"] - r1["min_x"]

    intersection_area = height * width
    union_area = (r1["max_x"] - r1["min_x"]) * (r1["max_y"] - r1["min_y"]) + (r2["max_x"] - r2["min_x"]) * (
        r2["max_y"] - r2["min_y"]
    )
    return intersection_area / union_area


def NMS(regions: List[Region], conf_threshold: float, iou_threshold: float):
    """
    Perform Non-Max Suppression on a list of regions.
    
    Parameters
    ----------
    regions: List[Region]
        All regions in the image independent of the classes
    conf_threshold: float
        Minimum classification confidence required for a region
    iou_threshold: float
        Maximum IOU score between two region to consider them BOTH as unique regions

    Returns
    -------
        A list of unique region for each class/category
    """
    # Sort the bounding boxes in descending order
    regions = sorted(regions, key=lambda x: x.conf_score, reverse=True)
    # Filter out the bounding boxes with confidence score less than conf_threshold
    regions = [r for r in regions if r.conf_score >= conf_threshold]
    # sorted the remaining boxes in desc order
    unique_regions = []
    # Repeat
    while len(regions) > 0:
        # Select first box (highest conf one)
        max_conf_region = regions.pop(0)
        # add to new list of unique boxes
        unique_regions.append(max_conf_region)
        # Repeat for all other boxes:
        for idx, box in enumerate(regions):
            # if class is not same as the first box, skip
            # if box.class_name != max_conf_region.class_name:
            #    continue
            # calculate iou with first box if the class is same as first box
            iou_score = IOU(max_conf_region, box)
            # if iou >= iou_threshold, skip the box else add to unique box list
            if iou_score < iou_threshold:
                # means the region is not having significant overlap so should be included for later pipeline stage
                unique_regions.append(box)

            regions.remove(box)

    return unique_regions

from torch import nn, Size, Tensor
from torchvision.ops import box_iou
from coco_obj_det import (
    GRID_CELL_COUNT,
    OBJECT_CATEGORIES,
    PER_CELL_BB_COUNT,
)
import torch


class YoloLoss(nn.Module):
    def __init__(
        self,
    ) -> None:
        super().__init__()

    def forward(self, predictions_batch: Tensor, target_batch: Tensor):
        assert target_batch.size()[1:] == Size(
            (
                GRID_CELL_COUNT,
                GRID_CELL_COUNT,
                (5 * PER_CELL_BB_COUNT + OBJECT_CATEGORIES + 1),
            )
        )
        assert predictions_batch.size()[1:] == Size(
            (
                GRID_CELL_COUNT,
                GRID_CELL_COUNT,
                (5 * PER_CELL_BB_COUNT + OBJECT_CATEGORIES + 1),
            )
        )

        # Predictions are sequences like [box1, box2, ..boxn, cat1, cat2, cat3...catk]
        # where each boxn is actually a sequence of 5 items as [x,y,w,h,confidence score]
        # But we need them in format [[box1, cat1, cat2, ...catk], [box2, cat1, cat2...catk] ...]
        # So we will create new tensor for itp = torch.empty(1, 2, 7)

        # pred_bbox_batch = pred_bbox_batch.reshape(-1, GRID_CELL_COUNT, GRID_CELL_COUNT, PER_CELL_BB_COUNT, 5)
        # true_bbox_batch = true_bbox_batch.reshape(-1, GRID_CELL_COUNT, GRID_CELL_COUNT, PER_CELL_BB_COUNT, 5)
        lambda_obj = 5
        lambda_noobj = 0.5

        batch_error = 0

        for all_true_bbox, all_pred_bbox in zip(target_batch, predictions_batch):
            total_error = 0
            for pred_cell, truth_cell in zip(
                all_pred_bbox.flatten(0, 1), all_true_bbox.flatten(0, 1)
            ):
                pred_bb = pred_cell[: -(OBJECT_CATEGORIES + 1)].reshape(-1, 5)
                truth_bb = truth_cell[: -(OBJECT_CATEGORIES + 1)].reshape(-1, 5)

                cell_error = 0
                if truth_cell[4] == 1 or truth_cell[9] == 1:
                    # means the cell contains an object
                    IoUs = box_iou(pred_bb[:, :4], truth_bb[:, :4])
                    # iou will contain elements which correspond to the the iou between
                    # [[pred1 & target, pred1 & target]
                    #  [pred2 & target, pred2 & target]]
                    # at these corresponding locations
                    _, location = torch.max(IoUs, dim=1)

                    # Now to get the best prediction bb, we only need whether it is row 1 or row 2 in pred_cell
                    # because second elements in each row will be the same as first element
                    # Hence, the IoUs will be same in each row so location[0][0] will also be same as location[0][1]
                    responsible_box = pred_bb[location[0], :]

                    #### Coordinate error ####
                    loc_err = torch.sum((responsible_box[:2] - truth_bb[0, :2]) ** 2)

                    #### Dimension error ####
                    # Width and height can be predicted negative by model so we need to take absolute values before taking root. else sqrt will give nan
                    dim_err = torch.sum(
                        (
                            torch.sign(responsible_box[2:4])                                # Preserving the sign of numbers as torch.abs is taken
                            * torch.sqrt(torch.abs(responsible_box[2:4]) + 1e-5)            # torch.sqrt() cannot take negative values else will output nan so torch.abs() is used
                                                                                            # 1e-5 is added for stability; else gradient of absolute of 0 (if comes) will be inifinity
                            - torch.sqrt(truth_bb[0, 2:4])                                  # torch.sqrt() with truth wont need abs since all width and height > 0
                        )
                        ** 2                                                                # taking square of difference in dimensions
                    )  
                    box_err = lambda_obj * (loc_err + dim_err)

                    #### object presence confidence error ####
                    obj_conf_err = (responsible_box[4] - truth_bb[0, 4]) ** 2

                    #### Class prediction error ####
                    class_pred_err = torch.sum(
                        (
                            pred_cell[5 * PER_CELL_BB_COUNT :]
                            - truth_cell[5 * PER_CELL_BB_COUNT :]
                        )
                        ** 2
                    )
                    cell_error = box_err + obj_conf_err + class_pred_err
                    # print(f"box_err {box_err}")
                    # print(f"obj_conf_err {obj_conf_err}")
                    # print(f"class_pred_err {class_pred_err}")
                else:
                    # means the cell does not contain any object
                    # So we shall make all the bounding box predictions' confidence score to 0 saying there is no object
                    cell_error = lambda_noobj * torch.sum(
                        (truth_bb[:, 4] - pred_bb[:, 4]) ** 2
                    )

                total_error += cell_error
            batch_error += total_error

        return batch_error

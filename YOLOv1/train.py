import os
from torch.optim import Adam
from yolo_loss import YoloLoss
import utils
import torch
from model import Yolov1, config
from coco_obj_det import (
    train_loader,
    GRID_CELL_COUNT,
    PER_CELL_BB_COUNT,
    OBJECT_CATEGORIES,
)
from torch.optim.lr_scheduler import StepLR, ReduceLROnPlateau
from torchmetrics.detection.mean_ap import MeanAveragePrecision
from torchvision import ops
IMG_CHANNELS = 3
IMG_SIZE = 448
NUM_EPOCHS = 15000

from torch.utils.tensorboard import SummaryWriter


if __name__ == "__main__":
    torch.autograd.set_detect_anomaly(True)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(f"Found device {device}")

    model = Yolov1(arch=config)
    optimizer = Adam(params=model.parameters(), lr=0.1)
    lr_scheduler = ReduceLROnPlateau(optimizer, mode='min', min_lr=0.00001, patience=50)
    loss_fn = YoloLoss()
    # default `log_dir` is "runs" - we'll be more specific here
    writer = SummaryWriter('runs/yolo')
    model.to(device=device)
    
    resume_epoch = 0
    best_loss = float('inf')

    if os.path.exists("model.pth"):
        checkpoint = torch.load("model.pth")
        model = model.load_state_dict(checkpoint['model'])
        optimizer = optimizer.load_state_dict(checkpoint['optimizer'])
        resume_epoch = checkpoint['epoch'] + 1
    
    for epoch in range(resume_epoch, NUM_EPOCHS):
        writer.add_scalar("Learning Rate", optimizer.param_groups[0]['lr'], global_step=epoch)
        model.train()
        train_loss = 0
        for idx, (imgs, annotations) in enumerate(train_loader):
            imgs, annotations = imgs.to(device), annotations.to(device)
            result = model(imgs)
            
            batch_loss = loss_fn(result, annotations)
            optimizer.zero_grad()
            batch_loss.backward()
            optimizer.step()
            
            train_loss += batch_loss.item()

            print(f"Epoch {epoch}: Loss of batch {idx} is {batch_loss}, LR: {optimizer.param_groups[0]['lr']}")

        train_loss /= idx               # Averaging out the loss over batches
        lr_scheduler.step(train_loss)
        writer.add_scalar("Training Loss", train_loss, global_step=epoch)
        for name, param in model.named_parameters():
            writer.add_histogram(name, param, global_step=epoch)

        if batch_loss < best_loss:
            best_loss = batch_loss
            torch.save({
                'optimizer': optimizer.state_dict(),
                'model': model.state_dict(),
                'epoch': epoch
            }, "model.pth")

        model.eval()
        with torch.no_grad():
            imgs, batch_annotations = next(iter(train_loader))
            imgs = imgs.to(device=device)
            # real_target = utils.cellboxes_to_boxes(
            #     annotations, GRID_CELL_COUNT, PER_CELL_BB_COUNT, OBJECT_CATEGORIES, IMG_SIZE
            # )
            batch_predictions = model(imgs).to(device=torch.device("cpu"))
            
            best_batch_predictions = []
            for prediction in batch_predictions:
                boxes = utils.best_boxes(prediction, PER_CELL_BB_COUNT, OBJECT_CATEGORIES)
                best_batch_predictions.append(boxes)
            
            batch_pred_bcsl = []
            for prediction in best_batch_predictions:
                # bcsl = box_confscore_labels
                pred_bcsl = utils.cellboxes_to_boxes(prediction, GRID_CELL_COUNT, IMG_SIZE)
                # Subject predictions to NMS to remove overlapping boxes
                box_to_keep = ops.nms(boxes=pred_bcsl[:, :4], scores=pred_bcsl[:, 4], iou_threshold=0.7)
                pred_bcsl = pred_bcsl[box_to_keep, :]
                batch_pred_bcsl.append(pred_bcsl)
            
            best_batch_annotations = []
            for img_annotations in batch_annotations:
                boxes = utils.best_boxes(img_annotations, PER_CELL_BB_COUNT, OBJECT_CATEGORIES)
                best_batch_annotations.append(boxes)
            
            batch_ann_bcsl = []
            for truth in best_batch_annotations:
                # bcsl = box_confscore_labels
                true_bcsl = utils.cellboxes_to_boxes(truth, GRID_CELL_COUNT, IMG_SIZE)
                batch_ann_bcsl.append(true_bcsl)
            assert pred_bcsl.size() <=  torch.Size((GRID_CELL_COUNT * GRID_CELL_COUNT, 6))
            
            predictions = [{'boxes': bcsl[:, :4], 'labels': bcsl[:, 5].to(dtype=torch.int8), 'scores': bcsl[:, 4]} for bcsl in batch_pred_bcsl]
            truths = [{'boxes': bcsl[:, :4], 'labels': bcsl[:, 5].to(dtype=torch.int8)} for bcsl in batch_ann_bcsl]
            
            map = MeanAveragePrecision(box_format='xywh')
            map.update(preds=predictions, target=truths)
            result = map.compute()
            print(result)
            writer.add_scalar("MAP", result.get("map_50"), epoch)
            grid = utils.create_annotated_img_grid(imgs, batch_ann_bcsl)
            writer.add_image("real_targets", grid, epoch)
            
            grid = utils.create_annotated_img_grid(imgs, batch_pred_bcsl)
            writer.add_image("predicted_targets", grid, epoch)


import sys
from torchvision.datasets import CocoDetection
from torchvision.transforms import (
    ToTensor,
    Normalize,
    Resize,
    Compose,
    ToPILImage,
)
from torch.utils.data import DataLoader, RandomSampler
from pprint import pprint
from torchvision.utils import draw_bounding_boxes
import torchvision
import torch
import torchvision.transforms.functional as F
import debug

IMG_CHANNELS = 3
IMG_SIZE = 448
BATCH_SIZE = 4
GRID_CELL_COUNT = 7
OBJECT_CATEGORIES = 90
PER_CELL_BB_COUNT = 2

import numpy as np
import matplotlib.pyplot as plt

plt.rcParams["savefig.bbox"] = "tight"


def show(imgs):
    imgs = [imgs]
    fig, axs = plt.subplots(ncols=len(imgs), squeeze=False)
    for i, img in enumerate(imgs):
        img = img.detach()
        img = F.to_pil_image(img)
        axs[0, i].imshow(np.asarray(img))
        axs[0, i].set(xticklabels=[], yticklabels=[], xticks=[], yticks=[])
    plt.imshow()


transforms = [
    ToTensor(),
    # Resize((IMG_SIZE, IMG_SIZE), antialias=None),
    # Normalize(
    #     mean=[(0.5,) * IMG_CHANNELS], std=[(0.5,) * IMG_CHANNELS]
    # ),
]

unique_anns = set()


def custom_collate(batch):
    imgs, batch_annotations = zip(*batch)

    filtered_batch = []
    for img, annotations in batch:
        filtered_annotations = []
        for annotation in annotations:
            if (annotation['bbox'][0] + annotation['bbox'][2]) > img.size(2):
                continue
            if (annotation['bbox'][1] + annotation['bbox'][3]) > img.size(1):
                continue

            filtered_annotations.append(annotation)
        filtered_batch.append((img, filtered_annotations))
    batch = tuple(filtered_batch)
    # resizing annotation coordinates if Resize used in transforms
    # because even coordinates of annotations will need to be adjusted as per the image
    for img, annotations in batch:
        # pprint(f"Image id: {annotations[0]['image_id']} has {len(annotations)} annotations")
        for x in annotations:
            if x.get('processed', False):
                continue
            x["bbox"][0] *= IMG_SIZE / img.size(2)
            x["bbox"][2] *= IMG_SIZE / img.size(2)
            x["bbox"][1] *= IMG_SIZE / img.size(1)
            x["bbox"][3] *= IMG_SIZE / img.size(1)
            x['bbox'] = list(map(int, x['bbox']))
            if any([c > IMG_SIZE for c in x['bbox']]):
                print("Printing within the first loop")
                print(x)
                print(IMG_SIZE)
                print(img.size())
                sys.exit()
            
            
    # create a tensor of resized image
    new_imgs = []
    for img in imgs:
        img = F.resize(img, size=(IMG_SIZE, IMG_SIZE), antialias=None)
        new_imgs.append(img)


    # Prepare annotations for YOLO format
    for img_idx, img_annotations in enumerate(batch_annotations):
        img_width, img_height = imgs[img_idx].size(2), imgs[img_idx].size(1)
        # width and height will be same coz the cell is SxS sized in Yolov1
        cell_width = cell_height = IMG_SIZE / GRID_CELL_COUNT

        for annotation in img_annotations:
            if annotation.get('processed', False):
                continue
            bbox_origin_x, bbox_origin_y, bbox_width, bbox_height = annotation['bbox']
            bbox_center = [ bbox_origin_x + (bbox_width // 2), bbox_origin_y + (bbox_height // 2) ]

            # annotation is in format [x,y,width,height] with aboslute values, not normalized
            annotation["bbox_center_within_cell"] = [
                round((bbox_center[0] % cell_width) / cell_width, 3),
                round((bbox_center[1] % cell_height) / cell_height, 3),
            ]

            annotation["bbox_dim"] = [
                round(bbox_width / img_width, 3),
                round(bbox_height / img_height, 3),
            ]

            annotation["cell_location"] = [
                int((bbox_center[0] // cell_width)),
                int((bbox_center[1] // cell_height)),   
            ]
            zr = torch.zeros(OBJECT_CATEGORIES + 1)

            zr[annotation["category_id"]] = 1
            bbox = [*annotation["bbox_center_within_cell"], *annotation["bbox_dim"], 1]
            bbox_clones = bbox * PER_CELL_BB_COUNT
            annotation["cell_target"] = torch.tensor(
                [
                    *bbox_clones,
                    *zr.tolist(),
                ]
            )
            annotation['processed'] = True
            # print(annotation['cell_location'])
            # print(annotation['bbox'])

    # SxSx(B*(5)+ #classes)
    batch_annotations_2 = []
    for img, img_annotations in zip(new_imgs, batch_annotations):
        img_target = torch.zeros(
            GRID_CELL_COUNT,
            GRID_CELL_COUNT,
            5 * PER_CELL_BB_COUNT + OBJECT_CATEGORIES + 1,
        )
        for annotation in img_annotations:
            x, y = annotation["cell_location"]
            try:
                img_target[x, y, :] = annotation["cell_target"]
            except IndexError:
                print("Caught index error on the annotation")
                print(annotation)
                img = img * 255
                debug.plot(img, annotation['bbox'])

        batch_annotations_2.append(img_target)

    return torch.stack(new_imgs), torch.stack(batch_annotations_2)


def test_annotation(img, annotations):
    img_width, img_height = img.size(2), img.size(1)

    cell_width = cell_height = img_width / GRID_CELL_COUNT
    print(cell_width)
    for x in annotations:
        cell_x, cell_y = x["cell_location"][0], x["cell_location"][1]
        x["bbox"] = [
            cell_x * cell_width + x["bbox_loc_within_cell"][0] * cell_width,
            cell_y * cell_height + x["bbox_loc_within_cell"][1] * cell_height,
            x["bbox_dim"][0] * img_width,
            x["bbox_dim"][1] * img_height,
        ]
    pprint(annotations)
    boxes = torch.tensor([x["bbox"] for x in annotations], dtype=torch.float)
    boxes = torchvision.ops.box_convert(boxes, in_fmt="xywh", out_fmt="xyxy")
    result = draw_bounding_boxes(
        torch.tensor(img * 255, dtype=torch.uint8),
        boxes,
        colors="blue",
        width=2,
    )
    result = ToPILImage()(result)
    result.show()


train_dataset = CocoDetection(
    root="C:/Users/Ayush/Downloads/train2017/train2017",
    annFile="C:/Users/Ayush/Downloads/annotations/annotations/instances_train2017.json",
    transform=Compose(transforms),
)


sampler = RandomSampler(train_dataset, num_samples=256, replacement=True)

train_loader = DataLoader(
    dataset=train_dataset,
    sampler=sampler,
    batch_size=BATCH_SIZE,
    collate_fn=custom_collate,
)


if __name__ == "__main__":
    for i in train_loader:
        pass


# imgs, annotations = next(iter(train_loader))
# pprint(annotations[0].size())
# boxes = torch.tensor([x["bbox"] for x in annotations[0]], dtype=torch.float)
# boxes = torchvision.ops.box_convert(boxes, in_fmt="xywh", out_fmt="xyxy")
# result = draw_bounding_boxes(
#     torch.tensor(imgs[0] * 255, dtype=torch.uint8),
#     boxes,
#     colors="green",
#     width=2,
# )
# result = ToPILImage()(result)
# result.show()
# test_annotation(imgs[0], annotations[0])

from torch import nn
from typing import Dict
from torchsummary import summary
from torch.utils.data import DataLoader
from coco_obj_det import (
    GRID_CELL_COUNT,
    OBJECT_CATEGORIES,
    PER_CELL_BB_COUNT,
)


config = [
    {
        "repeat": 1,
        "config": [{"in": 3, "out": 64, "pad": 3, "stride": 2, "kernel": 7}],
        "pool": True,
    },
    {
        "repeat": 1,
        "config": [{"in": 64, "out": 192, "pad": 1, "stride": 1, "kernel": 3}],
        "pool": True,
    },
    {
        "repeat": 1,
        "config": [
            {"in": 192, "out": 128, "pad": 0, "stride": 1, "kernel": 1},
            {"in": 128, "out": 256, "pad": 1, "stride": 1, "kernel": 3},
            {"in": 256, "out": 256, "pad": 0, "stride": 1, "kernel": 1},
            {"in": 256, "out": 512, "pad": 1, "stride": 1, "kernel": 3},
        ],
        "pool": True,
    },
    {
        "repeat": 4,
        "config": [
            {"in": 512, "out": 256, "pad": 0, "stride": 1, "kernel": 1},
            {"in": 256, "out": 512, "pad": 1, "stride": 1, "kernel": 3},
        ],
        "pool": False,
    },
    {
        "repeat": 1,
        "config": [
            {"in": 512, "out": 512, "pad": 0, "stride": 1, "kernel": 1},
            {"in": 512, "out": 1024, "pad": 1, "stride": 1, "kernel": 3},
        ],
        "pool": True,
    },
    {
        "repeat": 2,
        "config": [
            {"in": 1024, "out": 512, "pad": 0, "stride": 1, "kernel": 1},
            {"in": 512, "out": 1024, "pad": 1, "stride": 1, "kernel": 3},
        ],
        "pool": False,
    },
    {
        "repeat": 1,
        "config": [
            {"in": 1024, "out": 1024, "pad": 1, "stride": 1, "kernel": 3},
            {"in": 1024, "out": 1024, "pad": 1, "stride": 2, "kernel": 3},
        ],
        "pool": False,
    },
    {
        "repeat": 1,
        "config": [
            {"in": 1024, "out": 1024, "pad": 1, "stride": 1, "kernel": 3},
            {"in": 1024, "out": 1024, "pad": 1, "stride": 1, "kernel": 3},
        ],
        "pool": False,
    },
]

NUM_EPOCHS = 5


class Yolov1(nn.Module):
    def __init__(self, arch):
        super().__init__()

        self.arch = arch
        self.model = self.create_model()

    def create_model(self):
        network = []
        for block in self.arch:
            network.append(ConvBlock(block))
        network.extend(
            [
                nn.Flatten(),
                nn.Linear(GRID_CELL_COUNT * GRID_CELL_COUNT * 1024, 4096),
                nn.LeakyReLU(0.1),
                nn.Linear(
                    4096,
                    GRID_CELL_COUNT
                    * GRID_CELL_COUNT
                    * (5 * PER_CELL_BB_COUNT + OBJECT_CATEGORIES + 1),
                ),
                nn.Sigmoid()
            ]
        )
        return nn.Sequential(*network)

    def forward(self, x):
        result = self.model(x)
        result = result.reshape(
                -1,
                GRID_CELL_COUNT,
                GRID_CELL_COUNT,
                (5 * PER_CELL_BB_COUNT + OBJECT_CATEGORIES + 1),
            )
        return result


class ConvBlock(nn.Module):
    def __init__(self, arch: Dict[int, Dict]):
        super().__init__()

        layers = []
        for block_repeat in range(arch["repeat"]):
            for block in arch["config"]:
                cnn_layer = nn.Conv2d(
                    in_channels=block["in"],
                    out_channels=block["out"],
                    kernel_size=block["kernel"],
                    stride=block["stride"],
                    padding=block["pad"],
                )
                relu = nn.LeakyReLU(0.1)
                layers.extend([cnn_layer, relu])
            if arch["pool"]:
                layers.append(nn.MaxPool2d(kernel_size=2))

        self.model = nn.Sequential(*layers)

    def forward(self, x):
        return self.model(x)

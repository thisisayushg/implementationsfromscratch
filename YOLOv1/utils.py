import torch
import torchvision
import matplotlib.pyplot as plt
import numpy as np
from torchvision.utils import make_grid, draw_bounding_boxes
import torchvision.transforms.functional as F
import matplotlib.colors as mcolors
import random

def mean_average_precision(image_level_detections, true_boxes, iou_threshold):
    """
    MAP calculation for single class object detection
    """
    detections = []
    TP = torch.empty(0)
    FP = torch.empty(0)
    # Loop over every image
    box_counter = 0

    for idx in range(len(image_level_detections)):
        if true_boxes[idx]["boxes"].size(0) == 0:
            # no true detections exist
            continue
        # Creaet IoU matrix of (n x m) where
        # n=number of predicted boxes in current image
        # m= number of actaul annotated boxes in current image
        box_counter += len(image_level_detections[idx]["boxes"])

        iou_matrix = torchvision.ops.box_iou(
            image_level_detections[idx]["boxes"], true_boxes[idx]["boxes"]
        )

        # Filter the places where IoU is less than given threshold
        iou_matrix = torch.where(iou_matrix > iou_threshold, iou_matrix, 0)

        # Repeat the maximum element along each row in IoU matrix
        # For example,
        # input =  [[2, 3, 4],
        #           [1, 2, 3],
        #           [88, 9, 0]]
        # output = [[ 4,  4,  4],
        #           [ 3,  3,  3],
        #           [88, 88, 88]]

        temp = iou_matrix.max(dim=1).values.unsqueeze(1).repeat(1, iou_matrix.size(1))
        # mark only max along each row as 1, rest all as 0
        best_iou = torch.where((iou_matrix == temp) & (iou_matrix != 0), 1, 0)
        true_positives = best_iou.sum(dim=1)
        image_level_detections[idx]["true_positives"] = true_positives

        item = image_level_detections[idx]

        packed_item = zip(item["boxes"], item["scores"], item["true_positives"])
        # For every detection, this will become now a tuple of 3 items -> (box, score, true_positive?)
        detections.extend(packed_item)
    try:
        sorted_boxes = sorted(detections, key=lambda x: x[1], reverse=True)
    except ValueError:
        print("Detections after value error")
        print(detections)
    boxes, scores, true_positives = zip(*detections)

    true_positives = torch.tensor(true_positives)
    false_positives = (
        ~true_positives.bool()
    ).int()  # since only a single class object detection problem

    assert len(true_positives) == box_counter
    assert len(false_positives) == box_counter

    return true_positives, false_positives


def plot_prec_recall_curve(recalls, precisions):
    i = 1
    plt.figure(figsize=(32, 10))
    for threshold in np.arange(0.5, 0.95, 0.05):
        threshold = round(threshold, 2)
        plt.subplot(5, 2, i)
        plt.plot(recalls[threshold].cpu(), precisions[threshold].cpu())
        plt.xlabel("Recalls")
        plt.ylabel("Precision")
        plt.title(f"Precision vs Recall curve for threshold {threshold}")
        plt.show()
        i += 1

def cellboxes_to_boxes(img_predictions, GRID_CELL_COUNT, IMG_SIZE):
    """
    img_predicions of shape GRID_CELL_COUNT x GRID_CELL_COUNT x 6
    where each row = (x, y, w, h, conf score, label)
    """
    assert img_predictions.size() == torch.Size((GRID_CELL_COUNT, GRID_CELL_COUNT, 6))


    row_num = torch.arange(GRID_CELL_COUNT).reshape((-1, 1)).repeat(1, GRID_CELL_COUNT)
    col_num = torch.arange(GRID_CELL_COUNT).repeat(GRID_CELL_COUNT, 1)
    cell_width = cell_height = IMG_SIZE / GRID_CELL_COUNT

    center_x = (row_num + img_predictions[:, :, 0]) * cell_width
    center_y = (col_num + img_predictions[:, :, 1]) * cell_height

    box_widths = img_predictions[:, :, 2] * IMG_SIZE
    box_heights = img_predictions[:, :, 3] * IMG_SIZE

    origin_x = center_x - box_widths/2
    origin_y = center_y - box_heights/2

    denorm_img_predictions = torch.stack([origin_x, origin_y, box_widths, box_heights, img_predictions[:,:,4], img_predictions[:,:,5]])

    x = denorm_img_predictions.T.flatten(0, 1)
    # Filtering non-zero conf scored bounding boxes
    x = torch.index_select(x, 0, x[:, 5].nonzero().unique())

    return x

def create_annotated_img_grid(img_batch, box_batch):
    imgs_with_boxes = []
    colorset=list(mcolors.CSS4_COLORS.values())
    random.shuffle(colorset)

    imgs = img_batch.to(device=torch.device('cpu'))
    preds = [pred.to(device=torch.device('cpu')) for pred in box_batch]
    
    for img, output in zip(imgs, preds):
        boxes = torchvision.ops.box_convert(output[:, :4], in_fmt="xywh", out_fmt="xyxy")
        result = draw_bounding_boxes(
            torch.tensor(img * 255, dtype=torch.uint8),
            boxes,
            colors=colorset,
            width=2,
        )
        imgs_with_boxes.append(result)
    grid = make_grid(imgs_with_boxes)
    return grid

def show_img_annotations(imgs, preds):
    grid = create_annotated_img_grid(imgs, preds),
    plt.rcParams["savefig.bbox"] = 'tight'
    show(grid)
    plt.show()


def show(imgs):

    if not isinstance(imgs, list):
        imgs = [imgs]

    fig, axs = plt.subplots(ncols=len(imgs), squeeze=False)
    
    for i, img in enumerate(imgs):
        img = img.detach()
        img = F.to_pil_image(img)
        axs[0, i].imshow(np.asarray(img))
        axs[0, i].set(xticklabels=[], yticklabels=[], xticks=[], yticks=[])
    return fig, axs
    

def best_boxes(img_predictions, PER_CELL_BB_COUNT, OBJECT_CATEGORIES):
    '''
    Returns a GRID_CELL_COUNT x GRID_CELL_COUNT x 6 tensor
    '''
    # get the scores of each bounding box prediction. 
    # Shape of scores = PERCELL_BB_COUNT x GRID_CELL_COUNT x GRID_CELL_COUNT
    scores = torch.stack([img_predictions[:, :, i*5+5] for i in range(PER_CELL_BB_COUNT) ])
    # Get the indices of maximum scores
    idx = scores.argmax(dim=0)
    # idx shape = GRID_CELL_COUNT x GRID_CELL_COUNT
    # each element in idx is the bbox number which has the highest conf score
    # We need to extract that bounding box as img_predictions[:, :, i*5:i*5+5]
    best_boxes = torch.zeros(7, 7, 5)

    for i in range(PER_CELL_BB_COUNT):
        # create 2D matrix of i with shape GRID_CELL_COUNT x GRID_CELL_COUNT
        mask = torch.ones(7, 7) * i
        # make matrix 1 wherever same as the best scoring bb
        mask = mask == idx
        # preserve data of only bb having highest score; 0 otherwise
        # Transpose is necessary since the shape of img_predictions[:, :, i*5:i*5+5] is 
        # GRID_CELL_COUNT x GRID_CELL_COUNT x 5 which do not align with the numeric arrangement of mask
        # Tip: Debug by printing img_predictions[:, :, i*5:i*5+5] and mask
        per_cell_best_bb = (img_predictions[:, :, i*5:i*5+5].T * mask).T
        # preserve only nonzero values i.e. values of best bb
        # each bb is mututally exclusive as argmax returns one value per cell. 
        # So adding the tensors will do the trick
        best_boxes += per_cell_best_bb

    # Convert class predictions to labels
    class_preditions = img_predictions[:, :, -1 * (OBJECT_CATEGORIES+1):]
    labels = class_preditions.argmax(-1)

    # Append class labels to the end of each cell data
    return torch.cat([best_boxes, labels.unsqueeze(-1)], dim=-1)

    